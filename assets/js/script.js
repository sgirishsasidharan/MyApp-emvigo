$(document).ready(function () {
    $('.event-slide').owlCarousel({
        loop: true,
        nav: false,
        dots: true,
        items: 1
    });

    $('.top-navigation .popover-menu').on('click', function (e) {
        $(this).toggleClass('active');
    });
    $('.owl-carousel').owlCarousel({
        loop:true,
        margin:10,
        nav:false,
        items :1,
        autoplay : true
    });
    $('.page-content').css('height', window.innerHeight-120);
});